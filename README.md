![logo](logo.png)

Tsung - Docker Image
====================

[![Docker Pulls](https://img.shields.io/docker/pulls/castelislogice/tsung.svg)](https://hub.docker.com/r/castelislogice/tsung/)
[![Docker Stars](https://img.shields.io/docker/stars/castelislogice/tsung.svg)](https://hub.docker.com/r/castelislogice/tsung/)
[![PayPal donation](https://github.com/jaymoulin/jaymoulin.github.io/raw/master/ppl.png "PayPal donation")](https://www.paypal.me/jaymoulin)
[![Buy me a coffee](https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png "Buy me a coffee")](https://www.buymeacoffee.com/3Yu8ajd7W)
[![Become a Patron](https://badgen.net/badge/become/a%20patron/F96854 "Become a Patron")](https://patreon.com/jaymoulin)

(This product is available under a free and permissive license, but needs financial support to sustain its continued improvements. In addition to maintenance and stability there are many desirable features yet to be added.)

THIS REPOSITORY IS AUTO-UPDATED BY [GITHUB-RELEASE-NOTIFIER](https://github.com/femtopixel/github-release-notifier) (https://github.com/femtopixel/github-release-notifier)

# Prerequisites

## Initializing container

```
docker run -d --name tsung -v /path/to/local/folder/for/scenario:/root/.tsung/ -p 8090:8090 -p 8091:8091 castelislogice/tsung 
```

This will create a container with a running Tsung instance. You can then call `docker stop tsung` to stop it.

## Preparing scenario


1. Starting scenario recorder

Launch this command to start router to listen to your scenario

```
docker exec tsung tsung-recorder start
```

This will create a new scenario in your `/path/to/local/folder/for/scenario` defined in initialization.

2. Configure router

In your browser, configure proxy for all your calls (usually : Settings > Network > Proxy) with these values :
 - host : localhost
 - port : 8090
 
3. Write scenario

To write your scenario, just navigate though your website. Each action will be grabbed by the Tsung router and added to the scenario. 
Once finished, stop the record

Important note : Tsung cannot handle HTTPS request through HTTPS protocol. Replace it to http://- instead.

`E.g. https://www.google.com -> http://-www.google.com`

4. Stop scenario recorder

```
docker exec tsung tsung-recorder stop
```

Your scenario is now written in your folder!

# Running benchmark 

Just run this command

```
docker exec tsung tsung start
```

By default, it reads `tsung.xml` but you can use :

```
docker exec tsung tsung start -f <scenario>
```

with `<scenario>` the file of your scenario to test.

When running, you can check the dashboard right here http://localhost:8091

# Generating export

When everything went fine for your tests, You'll probably want to see graph report on your stats.

Just run this command to generate a report:

```
docker exec tsung tsung-report <log_file>
```

With `<log_file>` optional parameter to your log folder session. By default, it reads the latest folder in you log directory.

For example, for log `./log/20170609-1623`, just precise `20170609-1623`
