VERSION ?= v1.7.0
CACHE ?= --no-cache=1
FULLVERSION ?= v1.7.0
BUILDVERSION ?= v1.7.0
archs ?= arm32v7 amd64 i386 arm64v8

.PHONY: all build publish latest
all: build publish latest
qemu-aarch64-static:
	cp /usr/bin/qemu-aarch64-static .
qemu-arm-static:
	cp /usr/bin/qemu-arm-static .
build: qemu-aarch64-static qemu-arm-static
	$(foreach arch,$(archs), \
		cat Dockerfile | sed "s/FROM erlang:slim/FROM ${arch}\/erlang:slim/g" > .Dockerfile; \
		docker build -t castelislogice/tsung:${VERSION}-$(arch) --build-arg TSUNG_VERSION=${BUILDVERSION} -f .Dockerfile ${CACHE} .;\
	)
publish:
	docker push castelislogice/tsung
	cat manifest.yml | sed "s/\$$VERSION/${VERSION}/g" > manifest.yaml
	cat manifest.yaml | sed "s/\$$FULLVERSION/${FULLVERSION}/g" > manifest2.yaml
	mv manifest2.yaml manifest.yaml
	manifest-tool push from-spec manifest.yaml
latest: build
	FULLVERSION=latest VERSION=${VERSION} make publish
