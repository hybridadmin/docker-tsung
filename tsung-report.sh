#!/usr/bin/env bash

DIR=`pwd`
STATS='/usr/local/lib/tsung/bin/tsung_stats.pl'
if [ $# -eq 0 ]; then
    FOLDER=$(ls -1r "$DIR/log" | head -1)
else
    FOLDER=$1
fi

if [ ! -d "$DIR/report" ]; then
    mkdir "$DIR/report"
else
    rm "$DIR/report/*"
fi

cd "$DIR/report/" && $STATS --stats "$DIR/log/$FOLDER/tsung.log"
